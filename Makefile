all: hashing.o
	gcc main.c hashing.o -w -lm -o myht 

hashing.o: hashing.c hashing.h
	gcc -c hashing.c

clean:
	rm -f *.o

purge: clean
	rm -f myht
