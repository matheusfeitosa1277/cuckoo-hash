#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <search.h>

#include "hashing.h"

#define LINESIZE 1024
#define TAM 2

//M := Tam da tabela hash, especificacoes na pagina do professor

static void *root = NULL;

hashTable *t1, *t2;

static int cmp(const void *a, const void *b) {
    if ( *(int *) a < *(int *) b)
        return -1;
    if ( *(int *) a > *(int *) b)
        return 1;
    return 0;
}

static void action(const void *nodep, VISIT which, int depth) {
    int k = **(int **) nodep;
    int aux = *(int**)nodep;

    switch (which) {
        case preorder:
            break;
        case postorder: //Ordenado do menor para o maior
            cuckooSearch(t1, t2, k); 
            break;
        case endorder:
            break;
        case leaf:
            cuckooSearch(t1, t2, k);
            break;
   }
}

int main() {
    char *op, *lineBuf = malloc(sizeof(char) * LINESIZE);
    int k, *aux, **val;

    t1 = inicHashTable(M);
    t2 = inicHashTable(M);

    while (fgets(lineBuf, LINESIZE, stdin)) {
        op = strtok(lineBuf, " "); k = atoi(strtok(NULL, "\n"));

        int *ptr = malloc(sizeof(*ptr)); //"The calling program must store the actual data"
        *ptr = k;

        switch (*op) {
            case 'i' : 
                val = tsearch(ptr, &root, cmp);

                if (ptr != *val)
                    free(ptr);

                cuckooInsert(t1, t2, k);
                break;
            case 'r' :
                val = tfind(ptr, &root, cmp);
                
                if (val) {
                    aux = *val;
                    tdelete(*val, &root, cmp);
                    free(aux);
                }

                free(ptr);

                cuckooRemove(t1, t2, k);
                break;
        }
    }

    free(lineBuf); 

    twalk(root, action);
    tdestroy(root, free);

    freeHashTable(t1); freeHashTable(t2);

    return 0;
}

