#ifndef HASHING_H
#define HASHINH_H

#define M 11

typedef struct hashTable hashTable;

struct hashTable {
    int size, *array;
};

struct hashTable *inicHashTable(int size);

void freeHashTable(hashTable *T);

int h1(int k);

int h2(int k);

int cuckooInsert(hashTable *t1, hashTable *t2, int k);

int cuckooRemove(hashTable *t1, hashTable *t2, int k);

int cuckooSearch(hashTable *t1, hashTable *t2, int k);

void printCuckoo(hashTable *t1, hashTable *t2);

#endif

