#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "hashing.h"

/*
    Obs:
        So aceita naturais, pois h2 poderia retornar indices negativos se k * 0.9 > \FLO{k * 0.9}
*/

hashTable *inicHashTable(int size) {
    int i;
    hashTable *T = malloc(sizeof(hashTable));

    T->size = size;
    T->array = malloc(sizeof(int) * size);

    for (i = 0; i < size; i++)
        T->array[i] = -2;

    return T;
}

void freeHashTable(hashTable *T) {
    free(T->array); free(T);
}

int h1(int k) {
    return k % M;
}

int h2(int k) {
    return floor(M * ((float)k * 0.9 - floor((float)k * 0.9))); // Cast implicito
}

int (*pHash[])(int) = {h1, h2};

//  "Como não nos preocuparemos com rehashing, os casos de teste não terão colisão em T2."

int cuckooSearch(hashTable *t1, hashTable *t2, int k) {
    int i = h1(k), F = 0, T = 2;

    int *aux[] = {t1->array, t2->array};

    if (aux[F][i] == -2) // Chave nao existe
        return -2;

    do { 
        if (aux[F][i] == k) {
            printf("%d,T%d,%d\n", k, (F + 1), i);
            return i;
        }

        F ^= 1;
        i = pHash[F](k); //Ponteiro para funcoes hash
    } while (--T);

    return -2;

}

// Sobrescreve chaves repetidas

int cuckooInsert(hashTable *t1, hashTable *t2, int k) {
    int j, i = h1(k), F = 0;

    int *aux[] = {t1->array, t2->array};

    if (aux[F][i] == -2) // Insere T1
        return aux[F][i] = k;

    // Colisao em T1 -> Seguindo a notacao do enunciado
    j = h2(aux[F][i]);      // k_i = aux[F][i]
    aux[!F][j] = aux[F][i]; // Move T1 para T2
    
    return aux[F][i] = k;   // Inclui k_j

}

// -1 <- Excluido | -2 <- Slot Livre

int cuckooRemove(hashTable *t1, hashTable *t2, int k) {
    int i, j = h2(k), F = 1;

    int *aux[] = {t1->array, t2->array};

    if (aux[F][j] == k)  // Existe em T2 -> Apenas Remova
        return aux[F][j] = -2;

    i = h1(k);

    F ^= 1;

    if (aux[F][i] == k) // Existe em T1 -> Pos Excluida
        return aux[F][i] = -1;

    return -2;
}

/* Extras */
void printCuckoo(hashTable *t1, hashTable *t2) {
    int i;

    int *aux[] = {t1->array, t2->array};

    for (i = 0; i < M; i++) 
        printf("%2d | %2d | %2d\n", i, aux[0][i], aux[1][i]);

    putchar('\n');
}

