#!bin/bash

pushd testa

n=1

for file in ./*.in;
do
    echo "$file";
    ../myht < "$file" > file.sol;
    diff file.sol teste"$n".out;
    ((n++));
done

popd;
